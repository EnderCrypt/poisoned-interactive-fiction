"Poisoned" by "Magnus Gunnarsson"

Book 1 - Metadata

The story creation year is 2023.

The release number is 1.

use full-length room descriptions.



Book 2 - Definitions

Chapter 1 - directional info

Definition: a direction (called thataway) is viable if the room thataway from the location is a room.

Instead of going nowhere:
    let count of exits be the number of viable directions;
    if the count of exits is 0, say "You appear to be trapped in here." instead;
    if the count of exits is 1, say "From here, the only way is [list of viable directions].";
    otherwise say "From here, the viable directions are [list of viable directions].";

carry out looking:
	if stung is true:
		say "From here you can go [list of viable directions].";


Chapter 2 - using

Using it with is an action applying to one carried thing and one visible thing.
understand "use [something] on [something]" as using it with.
understand "use [something] with [something]" as using it with.
understand "use [something] in [something]" as using it with.

carry out using:
	say "Nothing interesting happends." instead.


Book 3 - Things

Chapter 1 - ingredients

an Ingredient is a kind of thing.
an ingredient is edible.

instead of eating ingredients:
	say "I suppose you COULD eat it... but why would you, why?";
	stop the action;


Chapter 2 - furniture

furniture is a kind of thing.
furniture is always scenery.

instead of taking furniture:
	say "No point in taking the [noun]." instead.


Book 4 - Backdrops

the beach is backdrop.
the description of the beach is "The beach sand is somewhat gravely, some corns are gray, some are sandy/yellowish, mixed in with soft round pebbles.".
understand "sand", "gravel" as the beach.

pebbles is backdrop.
the description of pebbles is "Small pebbles and stones litter the beach.".

the ocean is backdrop.
the description of the ocean is "The ocean raises and lowers very slowly on the beach, there isnt much wind to act on it.".
understand "water" and "sea" as the ocean.

faraway land is backdrop.
the description of the land is "The land is rather flat, almost entirely forest. there isnt alot of mountains (at least not that you can see through the fog)".

fog is backdrop.
the description of the fog is "The fog moves very slowly across the water, it will probalbly lift soon as the sun keeps rising.".

the sky is backdrop.
the description of the sky is "The sky is somewhat cloudy, a bit orangey due to the sunrise, though for the most part the morning has already gone past.".
the indefinite article of the sky is "the".

the ground is backdrop.
understand "dirt" as the ground.
the description of the ground is "Good for standing on.".
the ground is everywhere.

the players house is a region.
the ground is in the players house.

the outdoor is a region.
the ground, the sky is in the outside.

Book 5 - Locations


Chapter 1 - bedroom

Bedroom is a room. "Years ago when you built this house, perhaps you should have read up some on house construction. The walls are made out of a grid of simple wooden planks, many dry and warped... at least this place is always temperate and so you never have to freeze. Outside of your window you can hear the birds chirping.".

The printed name of the bedroom is "Your bedroom".
the bedroom is in the players house.
The Player is in the bedroom.

the window is scenery in the bedroom.
the description of the window is "The window frame is.. angled wrongly, by far one of your worst construction job back when you built this house, at least the glass is gorgeously see through.".
the indefinite article of the window is "the".

the birds is scenery in the bedroom.
understand "bird" as the birds.
the description of the birds is "Putting your head against the bottom of the window, you attempt to look up into the tree's outside your room[one of], though you cannot seem to spot the birds[or] and you manage to spot a [one of]small[or]big[or]cute[or]majestic[at random] singing [one of]red[or]blue[or]green[or]white[or]grey[at random] bird sitting on a branch at one of the trees[at random].".

instead of taking the birds:
	say "They are way too far away.";

instead of eating the birds:
	say "I suppose i could do that if i got ahold of one.";

An animal called a scorpion is in the bedroom.
the description of the scorpion is "Its a black, somewhat thickly scaled scorpion. it being fairly jumpy.. at least, for a scorpion, makes you very uncomfortable to get anywhere close to it..".
The scorpion is undescribed.

the bed is furniture in the bedroom. "A simple wood bed, with a very nice cotton filling, a blue blanket and a sky painted pillow.".
the indefinite article of the bed is "your".

before taking the scorpion:
	say "No way im touching it!";
	stop the action;

stung is a truth state that varies. stung is false.

the room description heading rule does nothing when stung is false.
the room description body text rule does nothing when stung is false.

carry out looking (this is the scorpion appearance rule):
	if stung is false:
		Say "Ouch! suddenly you wake up from a deep sting on your left arm.";
		Say "You look down and see a scorpion, a somewhat poisonous one too!";
		say "[line break]";
		say "Well thats just dandy isnt it, gonna need to make an antidote...";
		say "I'll need to mix together a [list of ingredients] in my alchemy room for this.";
		now the scorpion is described;
		now stung is true;
		continue the action;

a sting is part of the player.
understand "wound" as the sting.
the printed name of the sting is "stung".
the indefinite article of the sting is "a".
The description of the sting is "It hurts.".

instead of eating the sting:
	say "That wouldnt solve anything." instead;

every turn:
	if a random chance of 1 in 10 succeeds:
		say "Your wound [one of]itches[or]hurts[or]feels odd[or]is painful[or]looks really bad[at random].";


Chapter 2 - living room

The living room is a room. "Stepping into the living room, you feel somewhat relaxed despite the circumstances. theres a dresser with some photos positioned neatly ontop. In the corner is a sofa around a small stand with some plants in the middle.".
the living room is in the players house.
the printed name of the living room is "Your living room".
The living room is west of the bedroom.

The plant is furniture in the living room. "The plants are rather plain and simple, mixed variety of white, blue and red flowers seated in a decently big rectangular plant pot, along with some grass and moss growing near the soil".

the sofa is scenery in the living room. "Its your average brown leather sofa, fairly big actually, in a kinda J shape.".

the dresser is a supporter in the living room.
the description of the dresser is "My good ol dresser.".
the dresser is undescribed.
the dresser is fixed in place.

The photo is furniture on the dresser.
the description of the phoot is "The dresser is somewhat dilapitated and old, but also kinda majestic at the same time, in an old-fashioned way.".
understand "photoframe", "frame", "photos" or "photo frame" as the photo.

before taking photo:
	say "I see no reason to take them";
	stop the action;

the description of the photo is "This photo is of [one of]a cute little cat[or]a big fluffy dog[or]some family I dont know[or]my favourite food, noodles[or]a forest in some far away land[or]a beloved friend[or]the small garden you used to cultivate at your parents house[at random].".

the cat is an animal in the living room. "Your cat sits here".
the description of the cat is "Alsa is sucha cute cat.".
the indefinite article of the cat is "your".
the cat is wearing a nametag.
the description of the nametag is "Alsa".

check tasting the cat:
	say "The cats eyes widens in suprise as you lick its fur, it backs away from you." instead;

check squeezing the cat:
	say "[bold type]Squeeze Squeeze" instead;

check burning the cat:
	say "You monster, no!" instead;

check listening to the cat:
	say "putting your ears to the cats belly, you hear a steady purring sound." instead;

check taking the cat:
	say "She probably wouldnt like that." instead;

every turn when the player can see the cat:
	if the player is in a room (called players room) and the cat is in the players room:
		if a random chance of 1 in 5 succeeds:
			say "The cat meows [one of]softly[or]playfully[or]cutely[at random].";
			continue the action;

the small stand is furniture in the living room.
understand "table" as the small stand.
the description of the small stand is "A small stand for various things.".

Chapter 3 - Alchemy room

The alchemy room is a room. "Alchemy has always been your favourite hobby, far beyond everything else and as such this room has always been your favourite. the alchemy room is well built, the only thing looking out of place is the dirt floor, you figured it would be a good choice in case of any chemical spills. The rest of the room is filled with plethora of ingredients, potions and weird chemical creations you accidently made during many of your experiments.".
The printed name of the alchemy room is "Your alchemy room".
the alchemy room is in the players house.
the alchemy room is south of the living room.

dirt floor, shelves, chemical spills, bottled ingredients, bottled potions, chemical creations is furniture in the alchemy room.
understand "floor", "dirt" and "dirt floor" as the dirt floor.
the description of the dirt floor is "A simple dirt floor, hiding a layer of stone underneth, you replaced many parts of the dirt many times to keep any chemical spills from causing fumes.".
understand "spills" as the chemical spills.
the description of the spills is "You spilled chemicals here many times, many more than you'd like to admit, but being responsible you always replaced the dirt patches as soon as possible and so all the dirt is clean and comfortable to walk on.".
understand "ingredient" and "ingredients" as the bottled ingredients.
the description of the bottled ingredients is "Over the years you gathered an absolute ton (not literally) of ingredients, the most valuable and sensitive of which, you bottled up and put on racks here for the future, in case they'd ever become needed for any potion or experiment you'd wanna do.".
understand "shelf" and "rack" as the shelves.
the description of the shelves is "These 'shelves' as you like to call them is really just multiple simple planks hammered into the wall, luckily the bottled ingredients and potions are small and lightweight so there has never been any problem, at least not to this day.".
understand "potion" and "potions" as the bottled potions.
the description of the bottled potions is "A variety of potions you made, many arent really potions meant for drinking but instead further ingredients or chemicals to be used in other chemical processes.".
understand "creation" and "creations" as the chemical creations.
the description of the chemical creations is "Sometimes a chemical process would go haywire, either as a result of an accident or intentionally, this would create some beautiful chemical creations, weird colored gels, strange crystals, Gold nuggets, tree like structures of goo.".

the gels, crystals and nuggets is furniture in the alchemy room.
understand "structures of goo", "structure of goo", "gel", "weird gels", "weird colored gels", "colored gel" or "colored gels" as the gels.
the description of the gels is "The gel isnt so much of a gel anymore due to having hardened, but the beautiful color mix it is has you relax from the sheer simpleness of this thing.".
understand "strange", "crystal", "strange crystal" and "strange crystals" as the crystals.
the description of the crystals is "A single amalgamation of crystals, pointing in multiple directions, as you rotate it in front of a light it produces a variety of colors.".
understand "gold", "nugget", "nuggets" and "gold nugget" as the nuggets.
the description of the nuggets is "A set of gold nuggets you accidently created last month in your attempt to chemically produce artificial granola.".

a table is a supporter in the alchemy room.
the table is fixed in place.

an dried up well is a container in the alchemy room.

check inserting anything into the dried up well:
	say "I better not." instead.

an alchemy set is a device on the table.
the description of the alchemy set is "It contains a [list of things in the alchemy stomach].".

after looking in the alchemy room:
	if the alchemy stomach contains all the ingredients:
		say "Now you just gotta activate the alchemy set to make the antidote.";
	otherwise:
		say "To be able to make the antidote you'll need to use a [list of ingredients not in the alchemy stomach] on the alchemy set.";

instead of switching on the alchemy set:
	if the alchemy stomach contains all the ingredients:
		say "You activate the machine, it starts making a soft whirlling noise but eventually calms down, producing an antidote!";
		now the ingredients are nowhere;
		now the antidote is in the alchemy room;
	otherwise:
		say "This alone wouldnt make an antidote, you still need to add a [list of ingredients not in the alchemy stomach].";
	stop the action;

understand "activate [something]", "start [something]", "enable [something]" and "turn on [something]" as switching on.

an antidote is a thing.
the antidote is edible.

instead of eating the antidote, say "Its for drinking.".

instead of drinking the antidote:
	say "You drink the antidote and immediately feel great relief, your wound stops hurting.";
	end the story saying "Congratulations, you been cured!";


alchemy stomach is part of the alchemy set.
the alchemy stomach is a container and undescribed.

carry out using a ingredient with the alchemy set:
	now the noun is in the alchemy stomach;
	stop the action;

check switching on the alchemy set:
	if the noun does not contain all ingredients:
		say "It is not ready, i need to put in all the ingredients.";
		stop the action;


Chapter 4 - Kitchen

The kitchen is a room. "Despite being partially delapitated, the kitchen is fairly nice regardless, being tiled with fairly well kept tiles. The walls are mostly clean with occasional spots of unknown dried food splats.".
The kitchen is west from the living room.
the kitchen is in the players house.
The printed name of the kitchen is "Your kitchen".

A fridge is a closed openable container in the kitchen.
the description of the fridge is "A simple fridge, not great at keeping the cold, but lots of space.".
the fridge is fixed in place.

a hotdog is an ingredient in the fridge.
understand "soggy", "soggy hotdog" as the hotdog.
the printed name of the hotdog is "soggy hotdog".
the description of the hotdog is "Its been in the fridge for a few days, but should be a perfect key ingridient for the antidote.".

food splats is scenery in the kitchen.
the description of the food splats is "The food splats are old and dry at this point, should have cleaned it up earlier.".
understand "splat" and "splats" as the food splats.


Chapter 5 - Garden

outside of the living room is the garden.
the garden is in the outdoor.
the printed name of the garden is "The garden outside your house".
the description of the garden is "The morning sun shines softly across the roof of your house. Around you are plenty of plants you planted some months back along with various garden tools and other stuff you simply left around.".

garden tools is furniture in the garden. "Some trowels, shovels, buckets lay around here.".
the trowels, the bags, the buckets is furniture in the garden.
understand "throwel" as the trowels.
understand "bag" as the bags.
understand "bucket" as the buckets.

some plants is furniture in the garden.

a shovel is in the garden.
the shovel is undescribed.


Chapter 6 - The mountainside

the mountainside is east of the garden. "The mountain towers far into the sky, its steep enough that you wouldnt wanna climb it, but not quite steep enough to prevent at least some vegetation such as bushes and smaller trees from growing on it.".
the mountainside is in the outdoor.

the mountain is scenery in the mountainside. "Oy, this thing bloody huge I tell ya.".

a bug is a ingredient.
the bug is nowhere.
understand "chalk", "chalk beetle" and "beetle" as the bug.
the printed name of the bug is "chalk beetle".
the description of the bug is "A very nice speciment, like its name might suggest, its white as chalk albeit not as hard.". 

a rock is in the mountainside.
the printed name of the rock is "huge rock".
the description of the rock is "A massive rock [if standing]stands tall[else]laying to its side[end if], judging by the moss its been here for quite a while, probably fell down the mountain ages ago.".
the rock is fixed in place.
the rock can be standing or fallen, it is usually standing.

moss is scenery in the mountainside.
the description of the moss is "The moss is small, curly and tastes alot like salt...".

small trees is scenery in the mountainside.
understand "trees" as the small trees.
the description of the small trees is "Strangely small trees, growing on the side of the mountain in whatever stable creavices are available.".

small bushes is scenery in the mountainside.
understand "bushes" as the small bushes.
the description of the small bushes is "Very small and cute bushes, spread across the mountainside.".


check climbing the mountain:
	say "Its a bit too steep for that" instead;

check climbing the rock:
	say "a bit hard to get up ontop of the rock, but you are here now.. and theres nothing else here, so you climb back down again." instead.

check tasting the rock:
	say "Tastes like salty moss." instead.

check listening to the rock:
	say "I think i hear something crawling under it ..." instead.

carry out using the shovel with the rock:
	if the rock is standing:
		say "with great effort you pry the [noun] under the [second noun] and manage to make it fall over!";
		now the rock is fallen;
		say "Under its previous spot, you see a bunch of bugs, one particular bug catches your interest!";
		now the bug is in the mountainside;
		stop the action;
 

Chapter 7 - The clearing

The clearing is north of the garden. "The yellow grass waves slowly back and forth on the ground, in the distance surrounding the clearing a forest stands.".
the clearing is in the outdoor.

forest and grass is scenery in the clearing.
understand "yellow grass" as the grass.
the description of the forest is "The forest is rather plain, and not so dense.".

some butterflies is animal in the clearing.
the description of the butterflies is "Most of them are blue-white, some are red-brown, and very occasionally a couple of yellow ones are seen in the distance, the weather is fine and they seem to be having a good day, better than yours at least.".

instead of taking butterflies:
	say "You try catch the butterflies, but they dodge you effortlessly.".

instead of listening to butterflies:
	say "They flap their wings so softly that you only hear them whenever they accidently fly very very closeby." .


Chapter 8 - Foggy shore

The shore is north of the clearing. "The shore is somewhat gravely, and there is a soft fog drifting across the water. In the far distance you see some unknown land you always wanted to visit, but never had the courage to.".
the beach, pebbles, the ocean, the faraway land is in the shore.


Chapter 9 - River delta

The river delta is east of the shore. "From whitin the forest, a river flows out and into the ocean at this place, weaving its water inbetween the small islands of sand that the beach forms.".
the printed name of the beach river is "The river delta".
the beach, pebbles, the ocean, the faraway land is in the river delta.

the sand islands is scenery in the river delta. "Small patches of sand islands in the very shallow river.".
understand "sand island", "sand" and "island" as the sand islands.


Chapter 10 - The foresty river

The foresty river is south of the river delta. "The forest is split in half by a river, investigating more closely you can see that the river doesn't actually cut very deep into the ground. following the river with your gaze you can see how every now and then wet vegetation sprouts up from the ground.". 
the foresty river is in the outdoor.
The foresty river is east of the clearing.

a foresty-river-scenery, vegetation, fish and crayfish is scenery in the foresty river.
the printed name of the foresty-river-scenery is "river".
understand "river" as the foresty-river-scenery.
the description of the foresty-river-scenery is "The river is shallow but beautiful, occasionally a small fish or even crayfish is seen passing by.".
the description of the fish is "They are many small and greenish fishes.".
the description of the crayfish is "Crayfishes occasionally slowly move against the current.".
understand "sausage", "water sausage", "sprout" and "sprouts" as the vegetation.
the description of the vegetation is "Oh![line break]It is a [quotation mark]water sausage.[quotation mark]".

a toy horse is a thing.
understand "broken" as the toy horse.
the description of the toy horse is "broken toy horse".
instead of searching the foresty-river-scenery for the first time:
	say "Oh, theres a wet and half broken toy horse in the mud.";
	move the toy horse to the foresty river;
	stop the action;


Chapter 11 - the abandoned house

The abandoned house is a room.
the abandoned house is east of the foresty river. "This house has stood here for ages, even when you first moved to the land it was already here and looking practically just as bad as it looks now. The walls are completely ruined with holes everywhere[first time], it is shocking that a soft breeze hasn't made it crumble yet[only].".
the abandoned house is in the outdoor.
understand "abandoned" as the abandoned house.


Chapter 12 - the dilapidated room

inside from the abandoned house is the dilapidated room.
the description of the dilapidated room is "Entering the house immediately makes you uncomfortable, seeing how incredibly dilapidated everything is, its like the whole thing will fall down, burying you alive at any moment!"

a hole is in the dilapidated room.
the description of the hole is "Since you first found this house, this hole has always been here. being placed squarely in the middle of this room. it is half a meter wide and seems to go down several hundreds of meters into the ground. [one of]there is some light moving around down there.[or]there are some flickering lights down there.[or] gazing into the hole, you see only complete darkness.[at random]".

instead of entering the hole:
	end the story saying "You jump into the hole and fall to your doom.".
	

carry out using the toy horse with the hole:
	move the toy horse to the dwarf fortress;
	say "You look at the [noun] for a second, then the [second noun], the back... then drop it into the [second noun].[line break]You can hear it bouncing violently against the dirt walls of the hole, falling deeper and deeper down untill it lands.[line break]You pause for a moment looking down, but its too far to see anything...";
	change the down exit of the bedroom to the dwarf fortress;
	move the inquisitive dwarf to the bedroom;
	stop the action;


chapter 13 - the overgrown yard

the overgrown yard is south of the abandoned house. "Stepping around the abandoned house you enter its yard, if you could call it that. The whole place is completely overgrown, stinks, almost entirely turned into a swamp at this point!".
the overgrown yard is in the outdoor.

check smelling in the overgrown yard:
	say "It almost smells as bad as new york." instead.

a wall of vegetation is in the overgrown yard. "vegetation of unknown kinds grow everywhere here, forming an absolutely impenetrable wall.".
the description of the wall of vegetation is "As detestable as this area is, the plants also intrigue you.". 
understand "plant" and "plants" as the wall of vegetation.

the wall of vegetation is fixed in place.

a slimy nettle is a ingredient.

instead of taking the wall of vegetation:
	if the slimy nettle is nowhere:
		say "Theres way too much plants to take, or carry, though perhaps the plants may hide something useful." instead; 
	otherwise:
		say "It'd be pointless to try collect all these plants." instead;
	stop the action;

instead of climbing the wall of vegetation:
	say "You try climbing, but the plants you hold onto keep snapping, making you fall to the ground.";

instead of searching the wall of vegetation for the first time:
	say "Searching the vegetation, you find a slimy nettle!";
	now the slimy nettle is in the overgrown yard;
	stop the action;


Chapter 14 - Dwarf fortress

Dwarf fortress is a room. "A fairly dark fortress, carved out of the dirt. The place is decently well lit using torched pierced into the wall. All around you are plenty of dwarfs suspiciously eyeing you but not seeming to let your presence stop them from doing whatever it is they are doing.".
a Inquisitive dwarf is a person. "A dwarf with a very sturdy beard, with two 2-handed battle axed sheathed on his back.".

dwarfs is a thing in the dwarf fortress.
the description of the dwarfs is "so many dwarfs!".

torches is scenery in the dwarf fortress.
understand "torch" as the torches.
the description of the torches is "Suprisingly fancy archaic steel torches, perched on the dirt wall.".

the dirt wall is scenery in the dwarf fortress.
understand "dirt" as the dirt wall.
the description of the dirt wall is "This fortress was clearly cut out off the wall bit by bit using primitive tools, the walls are just dirt.".

carry out looking:
	if the player can see the inquisitive dwarf:
		if the toy horse is in the dwarf fortress:
			say "The dwarf peers his eyes suspiciously on you and goes:[line break]";
			say "[quotation mark]Aye, you dropped this toy horse on us[first time], for a moment i thought the old house owners were back.. but looks like not[only].[quotation mark][line break]";
			say "The dwarf drops the [toy horse] on the floor.";
			move the toy horse to the bedroom.

every turn when the player can see the inquisitive dwarf:
	say "The dwarf looks at [one of]you[or][a random thing which is not a person in the location][at random] inquisitively...";
	continue the action;


